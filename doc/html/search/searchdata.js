var indexSectionsWithContent =
{
  0: "abcdefghimnoqrstw~",
  1: "deftw",
  2: "ew",
  3: "abcdefghinoqrstw~",
  4: "bcdehimnrtw",
  5: "tw",
  6: "demnosw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator"
};

