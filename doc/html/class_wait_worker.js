var class_wait_worker =
[
    [ "WaitWorker", "class_wait_worker.html#ab775487e06899dc74d8992c996606f8f", null ],
    [ "~WaitWorker", "class_wait_worker.html#a767f19b8b71eff239a9481ede483dee4", null ],
    [ "quitFinished", "class_wait_worker.html#a5f65bc5e4691e9542afef8df55bf27be", null ],
    [ "stop", "class_wait_worker.html#af01864effa7662c8261128ff12fb922d", null ],
    [ "addWaitTask", "class_wait_worker.html#adaff9ea88795fa9d902711c1952828cd", null ],
    [ "addDelayedWaitTask", "class_wait_worker.html#a71dec4dbc5a123f20d0a5a7f707ed5a3", null ],
    [ "waitWorkerLoop", "class_wait_worker.html#a434d284c0e08697979b5c8c1a53fd529", null ],
    [ "quitLoop", "class_wait_worker.html#a52b9f53cccaa737e7de6d978c96ff9ab", null ],
    [ "timer", "class_wait_worker.html#a87df3552b89aad0db75385398426a9c9", null ],
    [ "waitTasks_", "class_wait_worker.html#a7fcda880e64348cdd84e2ebbb9926447", null ],
    [ "markedForDelete", "class_wait_worker.html#affa716c2d16efa5df8492f21f11f3a2d", null ],
    [ "refreshRate_", "class_wait_worker.html#a088f21300ece1749e1e7e02a130275df", null ]
];