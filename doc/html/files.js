var files =
[
    [ "appcore.h", "appcore_8h_source.html", null ],
    [ "appstate.h", "appstate_8h_source.html", null ],
    [ "centralwidget.h", "centralwidget_8h_source.html", null ],
    [ "exptask.h", "exptask_8h_source.html", null ],
    [ "exptasklist.h", "exptasklist_8h_source.html", null ],
    [ "exptasks.h", "exptasks_8h_source.html", null ],
    [ "lockableqvector.h", "lockableqvector_8h_source.html", null ],
    [ "mainwindow.h", "mainwindow_8h_source.html", null ],
    [ "plotproxy.h", "plotproxy_8h_source.html", null ],
    [ "serialport.h", "serialport_8h_source.html", null ],
    [ "stagecontrol.h", "stagecontrol_8h_source.html", null ],
    [ "timespan.h", "timespan_8h_source.html", null ],
    [ "usmcvb_com.h", "usmcvb__com_8h_source.html", null ],
    [ "waittask.h", "waittask_8h_source.html", null ],
    [ "waittask_old.h", "waittask__old_8h_source.html", null ],
    [ "waittasklist.h", "waittasklist_8h_source.html", null ],
    [ "waittasks.h", "waittasks_8h_source.html", null ],
    [ "winspec.h", "winspec_8h_source.html", null ],
    [ "winspecvb.h", "winspecvb_8h_source.html", null ]
];